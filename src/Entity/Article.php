<?php

namespace App\Entity;

class Article{
  public $id;
  public $titre;
  public $date;
  public $commentaire;

  public function fromSQL(array $sql){
    dump($sql);
    $this->id = $sql["id"];
    $this->titre = $sql["titre"];
    $this->date = \DateTime::createFromFormat('Y-m-d h:i:s', $sql["date"]);
    $this->commentaire = $sql["commentaire"];


  }
}