<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Article;

class ArticlePageController extends Controller
{
    /**
     * @Route("/article-page/{id}", name="article_page")
     */
    public function index(int $id, ArticleRepository $repo)
    {
        $article= $repo->getById($id);
        return $this->render('article_page/index.html.twig', [
           'article' => $article,
        ]);
    }
}
