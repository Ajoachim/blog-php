<?php

namespace App\Repository;

use App\Entity\Article;
use App\Utils\ConnectUtil;


class ArticleRepository{

  public function getAll():array{
  $articles = [];
   try {
    $cnx = ConnectUtil::getConnection();
    $query = $cnx->prepare("SELECT * FROM article ORDER BY id DESC");

    $query->execute();

    foreach ($query->fetchAll() as $row) {
      $article = new Article();
      $article->fromSQL($row);
      $articles[] = $article;
  }

} catch (\PDOException $e) {
  dump($e);
}
return $articles;
}

public function add(Article $article)
{
    
    try {
        $cnx = ConnectUtil::getConnection();

        $query = $cnx->prepare("INSERT INTO article ( titre, date, commentaire) VALUES ( :titre, :date, :commentaire)");
        
       
        $query->bindValue(":titre", $article->titre);
        $query->bindValue(":date", $article->date->format("Y-m-d h:i:s"));
        $query->bindValue(":commentaire", $article->commentaire);

        $query->execute();

        $article->id = intval($cnx->lastInsertId());

    } catch (\PDOException $e) {
        dump($e);
    }



  }

 
   public function update(Article $article){
     try{
       $cnx = ConnectUtil::getConnection();
     
       $query = $cnx->prepare("UPDATE article SET titre=:titre, date=:date, commentaire=:commentaire WHERE id=:id
       ");
     
       $query->bindValue(":titre", $article->titre);
       $query->bindValue(":date", $article->date->format("Y-m-d h:i:s"));
       $query->bindValue(":commentaire", $article->commentaire);
       $query->bindValue(":id", $article->id);
     
       return $query->execute();
     
      
     }catch (\PDOException $e){
       dump($e);
     } 
   return false;
   }
   
   public function delete(int $id){
     try{
      $cnx = ConnectUtil::getConnection();
     
       $query = $cnx->prepare("DELETE FROM article WHERE id=:id
       ");
     
      
       $query->bindValue(":id", $id);
     
       return $query->execute();
     
      
     }catch (\PDOException $e){
       dump($e);
     } 
   return false;
   }
   public function getById(int $id): ?Article{
    try{
      $cnx = ConnectUtil::getConnection();
     
    
      $query = $cnx->prepare("SELECT * FROM article WHERE id=:id
      ");
    
      
      $query->bindValue(":id", $id);
    
     $query->execute();
    
     $result = $query->fetchAll();
     
     if(count($result) === 1){
       $article =  new Article();
       $article->fromSQL($result[0]);
       return $article;
     }
     
    }catch (\PDOException $e){
      dump($e);
    } 
  return null;
  }

}

