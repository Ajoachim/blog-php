<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use App\Form\ArticleType;
use App\Entity\Article;


class ArticleController extends Controller
{
    /**
     * @Route("/article", name="article")
     */
    public function index(ArticleRepository $repo, Request $request)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $article = $form->getData();
            $repo->add($article);
            return $this->redirectToRoute("home");
        }
        return $this->render('article/add-article.html.twig', [
        //  'controller_name' => 'ArticleController',
             'article' => $article,
            'form' => $form->createView()
        ]);
    }

      /**
     * @Route("/article/remove/{id}", name="remove_article")
     */
    public function remove(int $id, ArticleRepository $repo){
        $repo-> delete($id);
       return $this->redirectToRoute("home");
    }

    /**
     * @Route("/article/update/{id}", name="update")
     */
    public function up(int $id, ArticleRepository $repo, Request $request){
        $article = $repo->getById($id);

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repo->update($form->getData());
            return $this->redirectToRoute("home");
        }

        return $this->render('article/update-article.html.twig', [
            "form" => $form->createView(),
            "article" => $article
        ]);
    }

    }



