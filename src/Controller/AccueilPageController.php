<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use App\Entity\Article;



class AccueilPageController extends AbstractController{
  /**
   * @Route("/home", name="home")
   */

  public function index(ArticleRepository $repo){
    $result = $repo->getAll();
    return $this->render("accueil.html.twig",[
      "result" => $result
    ]);
  }
}