-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: db_blog
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(45) NOT NULL,
  `date` datetime NOT NULL,
  `commentaire` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (6,'Les Ã©toiles','2018-07-22 00:00:00','Le sens premier du mot Ã©toile est celui d\'un point lumineux dans le ciel nocturne, et par extension, des figures gÃ©omÃ©triques reprÃ©sentant des rayons partant d\'un centre (voir le symbole de l\'Ã©toile). En astronomie, la signification scientifique plus restreinte d\'Ã©toile est celle d\'un corps cÃ©leste plasmatique qui rayonne sa propre lumiÃ¨re par rÃ©actions de fusion nuclÃ©aire, ou des corps qui ont Ã©tÃ© dans cet Ã©tat Ã  un stade de leur cycle de vie, comme les naines blanches ou les Ã©toiles Ã  neutrons1. Cela signifie qu\'ils doivent possÃ©der une masse minimale pour que les conditions de tempÃ©rature et de pression au sein de la rÃ©gion centrale â€” le cÅ“ur â€” permettent l\'amorce et le maintien de ces rÃ©actions nuclÃ©aires, seuil en deÃ§Ã  duquel on parle d\'objets substellaires. Les masses possibles des Ã©toiles s\'Ã©tendent de 0,085 masse solaire Ã  une centaine de masses solaires. La masse dÃ©termine la tempÃ©rature et la luminositÃ© de l\'Ã©toile.'),(7,'VÃ©nus','2018-07-25 10:52:00','La distance de VÃ©nus au Soleil est comprise entre 0,718 et 0,728 UA, avec une pÃ©riode orbitale de 224,7 jours. VÃ©nus est une planÃ¨te tellurique, comme le sont Ã©galement Mercure, la Terre et Mars. Elle possÃ¨de un champ magnÃ©tique trÃ¨s faible et n\'a aucun satellite naturel. Elle et Uranus sont les deux seules planÃ¨tes du SystÃ¨me solaire dont la rotation est rÃ©trograde. Elle est la seule ayant une pÃ©riode de rotation (243 jours) supÃ©rieure Ã  sa pÃ©riode de rÃ©volution. VÃ©nus prÃ©sente en outre la particularitÃ© d\'Ãªtre quasiment sphÃ©rique â€” son aplatissement peut Ãªtre considÃ©rÃ© comme nul â€” et de parcourir l\'orbite la plus circulaire des planÃ¨tes du SystÃ¨me solaire, avec une excentricitÃ© orbitale de 0,0068 (contre 0,0167 pour la Terre).'),(8,'Saturne','2018-07-26 09:48:00','Saturne est la sixiÃ¨me planÃ¨te du SystÃ¨me solaire par ordre de distance au Soleil et la deuxiÃ¨me aprÃ¨s Jupiter tant par sa taille que par sa masse1,2,3.\r\n\r\nSaturne est une planÃ¨te gÃ©ante, au mÃªme titre que Jupiter, Uranus et Neptune, et plus prÃ©cisÃ©ment une gÃ©ante gazeuse4,5 de type Jupiter froid comme Jupitera. D\'un diamÃ¨tre d\'environ neuf fois et demi celui de la Terre, elle est majoritairement composÃ©e d\'hydrogÃ¨ne et d\'hÃ©lium. Sa masse vaut 95 fois celle de la Terre6 et son volume 900 fois celui de notre planÃ¨te1. Sa pÃ©riode de rÃ©volution est d\'environ 29 ans. Elle Ã©tait au pÃ©rihÃ©lie le 26 juillet 20037 et Ã  l\'aphÃ©lie le 17 avril 20188.\r\n\r\nSaturne a un Ã©clat bien plus faible que celui des autres planÃ¨tes observables Ã  lâ€™Å“il nu. Sa magnitude apparente peut atteindre lors de l\'opposition un maximum de 0,439, tandis que son diamÃ¨tre apparent varie de 14,5 Ã  20,5 secondes d\'arc et que sa distance Ã  la Terre varie de 1,66 Ã  1,20 milliard de kilomÃ¨tres9.\r\n\r\nSaturne possÃ¨de un systÃ¨me d\'anneaux, composÃ©s principalement de particules de glace et de poussiÃ¨re. Saturne possÃ¨de de nombreux satellites, dont cinquante-trois ont Ã©tÃ© confirmÃ©s et nommÃ©s. Titan est le plus grand satellite de Saturne et la deuxiÃ¨me plus grande lune du SystÃ¨me solaire aprÃ¨s GanymÃ¨de autour de Jupiter. Titan est plus grand que la planÃ¨te Mercure et est la seule lune du SystÃ¨me solaire Ã  possÃ©der une atmosphÃ¨re significative.\r\n\r\nPlus lointaine des planÃ¨tes du SystÃ¨me solaire observables Ã  l\'Å“il nu dans le ciel nocturne depuis la Terre10, elle est connue depuis la PrÃ©histoire11.');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-26 16:08:20
